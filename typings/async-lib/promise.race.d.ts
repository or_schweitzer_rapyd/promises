export declare function race(promises: Promise<any>[]): Promise<any>;
export declare function some(promises: Promise<any>[], num: number): Promise<any[]>;
