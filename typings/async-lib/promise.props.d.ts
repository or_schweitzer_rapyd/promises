import * as myTypes from "./globalTypes.js";
export declare function props(promisesObj: {
    [k: string]: Promise<any>;
}): Promise<myTypes.Iobj>;
