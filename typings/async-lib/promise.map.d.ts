import * as myTypes from "./globalTypes.js";
export declare function mapParallel(iterable: myTypes.PromisesArr, cb: (item: Promise<any> | any) => Promise<any>): Promise<any[] | string>;
export declare function mapSeries(iterable: myTypes.PromisesArr, cb: (item: Promise<any> | any) => Promise<any>): Promise<any[] | string>;
