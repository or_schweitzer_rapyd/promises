import * as myTypes from "./globalTypes.js";
export declare function filterSeries(iterable: myTypes.PromisesArr, cb: (item: Promise<any> | string) => Promise<boolean>): Promise<any[] | string>;
export declare function filterParallel(iterable: myTypes.PromisesArr, cb: (item: Promise<any> | any) => Promise<boolean>): Promise<any[] | string>;
