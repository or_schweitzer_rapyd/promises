import * as myTypes from "./globalTypes.js";
export declare function each(iterable: myTypes.PromisesArr, cb: (item: Promise<any> | any) => Promise<any>): Promise<any[] | string>;
