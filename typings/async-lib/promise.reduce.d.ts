import * as myTypes from "./globalTypes.js";
export declare function reduce<T>(iterable: myTypes.PromisesArr, cb: ((acc: Promise<T> | T, item: any) => Promise<T>), initial?: T): Promise<T>;
