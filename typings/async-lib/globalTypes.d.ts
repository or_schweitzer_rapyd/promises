export interface Iobj {
    [k: string]: any;
}
export declare type PromisesArr = Promise<any>[] | any[] | string;
