export interface Iobj {
    [k: string]: any;
}

export type PromisesArr = Promise<any>[] | any[] | string ;