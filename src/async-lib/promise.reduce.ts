import * as myTypes from "./globalTypes.js";

export async function reduce<T>(
    iterable: myTypes.PromisesArr,
    cb: ((acc: Promise<T> | T, item: any)=> Promise<T>),
    initial?: T
): Promise<T> {
    let aggregator = initial=== undefined ? iterable[0] : initial;

    for (const [i,item] of (iterable as (Promise<any>[] | any[])).entries()) {
        if((i === 0) && (initial === undefined)){
            continue;
        }
        aggregator = await cb(aggregator, item);
    }

    return aggregator;
}
