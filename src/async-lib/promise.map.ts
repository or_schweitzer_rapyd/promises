import * as myTypes from "./globalTypes.js";
//--------------------------------------------------

export async function mapParallel(
    iterable: myTypes.PromisesArr,
    cb: (item: Promise<any>|any) => Promise<any>
): Promise<any[] | string> {
    const results = [];
    const pending = (typeof iterable === "string") ?
    Array.from(iterable as string, (item) => cb(item)) :  Array.from(iterable, (item) => cb(item));

    for (const p of pending) {
        results.push(await p);
    }
    return results;
}

//--------------------------------------------------

export async function mapSeries(
    iterable: myTypes.PromisesArr,
    cb: (item: Promise<any> | any) => Promise<any>
): Promise<any[] | string> {
    const results = [];
    for (const item of iterable) {
        results.push(await cb(item));
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------
