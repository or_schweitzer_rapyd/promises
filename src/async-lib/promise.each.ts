import * as myTypes from "./globalTypes.js";
//--------------------------------------------------

export async function each(
    iterable: myTypes.PromisesArr,
    cb: (item: Promise<any> | any) => Promise<any>
): Promise<any[]|string> {
    for (const item of iterable) {
        await cb(item);
    }
    return iterable;
}

//--------------------------------------------------
