import * as myTypes from "./globalTypes.js";

//--------------------------------------------------

export async function filterSeries(
    iterable: myTypes.PromisesArr,
    cb: (item: Promise<any> | string) => Promise<boolean>
): Promise<any[] | string> {
    const results = [];
    for (const item of iterable) {
        if ((await cb(item)) === true) results.push(item);
    }
    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------

export async function filterParallel(
    iterable: myTypes.PromisesArr,
    cb: (item: Promise<any> | any) => Promise<boolean>
): Promise<any[] | string> {
    const results = [];
    const pending = (typeof iterable === "string") ?
    Array.from(iterable as string, (item) => cb(item)) :  Array.from(iterable , (item) => cb(item));
    
    for (const [i, p] of pending.entries()) {
        if ((await p) === true) results.push(iterable[i]);
    }

    return typeof iterable === "string" ? results.join("") : results;
}

//--------------------------------------------------
