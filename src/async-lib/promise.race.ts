export async function race(promises: Promise<any>[]): Promise<any> {
    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            resolve(await p);
        });
    });
}

//--------------------------------------------------
// same here - you have to use the Promise object
export async function some(
    promises: Promise<any>[],
    num: number
): Promise<any[]> {
    const results: any[] = [];
    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            results.push(await p);
            if (results.length === num) resolve(results);
        });
    });
}
