import { expect } from "chai";
import { race,some } from "../src/async-lib/promise.race";
import { echo } from "../src/async-lib/promise.utils";
describe("Basic Mocha String Test", function () {
    
    context("#race", () => {
        it("should exist", () => {
            expect(race).to.be.a("function");
        });
        it("should return first", async () => {
            const actual = await race([
                echo("third",300),
                echo("second",200),
                echo("first",100)
                ]
                 );
            expect(actual).to.equal("first");
        });
    });


    context("#some", () => {
        it("should exist", () => {
            expect(some).to.be.a("function");
        });
        it("should return first", async () => {
            const actual = await some([
                echo("third",300),
                echo("second",200),
                echo("first",100)
                ],2
                 );
            expect(actual).deep.equal(["first","second"]);
        });
    });

});


