import { expect } from "chai";
import { mapParallel,mapSeries } from "../src/async-lib/promise.map";
import { echo } from "../src/async-lib/promise.utils";
describe("Basic Mocha String Test", function () {
    
    context("#mapSeries", () => {
        it("should exist", () => {
            expect(mapSeries).to.be.a("function");
        });
        it("should increment each item in the array", async () => {
            const arr:any[] = [];
            const actual = await mapSeries([1,2,3], async (x:number) => {
                arr.push(x);
                return x +1;});

            //check the execution is not parallel
            expect(arr).to.deep.equal([1,2,3]);
            
            //check the output of mapSeries
            expect(actual).to.deep.equal([2,3,4]);
        });
             
    });

    context("#mapParallel", () => {
        it("should exist", () => {
            expect(mapParallel).to.be.a("function");
        });
        it("should increment each item in the array", async () => {
            const begin = Date.now();
            const arr:any[] = [];
            const actual = await mapParallel([1,2,3], async (x:number) => {
                echo("do nothing",200);
                return x +1;});

            const end = Date.now();

             //check it's done in parallel
             console.log(begin);
             console.log(end);
             console.log(end - begin);
             expect(end - begin).to.be.below(600);

            //check the output of mapParallel
            expect(actual).to.deep.equal([2,3,4]);
        });
             
    });



});



