
import { expect } from "chai";
import { all } from "../src/async-lib/promise.all";

describe("Basic Mocha String Test", function () {
    
    context("#all", () => {
        it("should exist", () => {
            expect(all).to.be.a("function");
        });
        it("should wait for all to resolve", async () => {
            const actual = await all([
                new Promise((res) => res(1)),
                new Promise((res) => res(2)),
                 new Promise((res) => res(3))]
                 );
             expect(actual).to.deep.equal([1,2,3]);
        });
    });

});


