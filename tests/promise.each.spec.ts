import { expect } from "chai";
import { each } from "../src/async-lib/promise.each";
describe("Basic Mocha String Test", function () {
    
    context("#each", () => {
        it("should exist", () => {
            expect(each).to.be.a("function");
        });
        it("should execute one by one", async () => {
            const actual :any[] = [];
            const f = async (item : any)=>{
                setTimeout(()=>{
                console.log("");
                }, 500);
                actual.push(item);
            };
           await each([1,2,3],f);
            expect(actual).to.deep.equal([1,2,3]);
        });
    });

});


