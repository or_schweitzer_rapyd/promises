import { expect } from "chai";
import { props } from "../src/async-lib/promise.props";
import {echo} from "../src/async-lib/promise.utils";
describe("Basic Mocha String Test", function () {
    
    context("#props", () => {
        it("should exist", () => {
            expect(props).to.be.a("function");
        });
        it("should wait for all to resolve", async () => {
            const actual = await props({
                "1" :echo("first",1000),
                "2" :echo("second",1100),
                "3" :echo("third",1200)
            });
             expect(actual).to.deep.equal({
                 "1" : "first",
                 "2" : "second",
                 "3" : "third"
                });
        });
    });

});


