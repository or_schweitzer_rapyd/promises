import { expect } from "chai";
import { filterParallel,filterSeries } from "../src/async-lib/promise.filter";
import { echo,delay } from "../src/async-lib/promise.utils";
describe("Basic Mocha String Test", function () {
    
    context("#filterSeries", () => {
        it("should exist", () => {
            expect(filterSeries).to.be.a("function");
        });
        it("should filter only number two", async () => {
            const arr:any[] = [];
            const actual = await filterSeries([
                //new Promise(res => res(1)),
                //new Promise(res => res(2)),
                //new Promise(res => res(3))], 
               1,2,3],
                async (x) => {
                    const num = await x;
                    arr.push(num);
                    console.log(`num is ${num}`);
                    return (num === 2);
                });

            //check the execution is not parallel
            expect(arr).to.deep.equal([1,2,3]);
            
            //check the output of filterSeries
            expect(actual).to.deep.equal([2]);
        });
             
    });

    context("#fiterParallel", () => {
        it("should exist", () => {
            expect(filterParallel).to.be.a("function");
        });
        it("should increment each item in the array", async () => {
            const begin = Date.now();
            const arr:any[] = [];
            const actual = await filterParallel([1,2,3], async (x) => {
                echo("do nothing",200);
                return ((await x) ===2);});

            const end = Date.now();
            
             //check it's done in parallel
             console.log(begin);
             console.log(end);
             console.log(end - begin);
             expect(end - begin).to.be.below(600);

            //check the output of filterParallel
            expect(actual).to.deep.equal([2]);

           
        });
             
    });



});



