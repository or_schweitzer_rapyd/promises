import { expect } from "chai";
import { reduce } from "../src/async-lib/promise.reduce";
import { echo } from "../src/async-lib/promise.utils";
describe("Basic Mocha String Test", function () {
    
    context("#reduce", () => {
        it("should exist", () => {
            expect(reduce).to.be.a("function");
        });
        it("shoud resolve to 6", async () => {
            const arr:any[] = [];
            const actual = await reduce([
                new Promise (res => res(1)),
                new Promise (res => res(2)),
                new Promise (res => res(3))],
                async (acc,item:Promise<number>) => {
                return (await (acc as Promise<number>) + (await item));});
            
            //check the output of mapSeries
            expect(actual).to.equal(6);
        });
             
    });

    



});



